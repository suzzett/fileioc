/*********************************************************
 * 
 * Programmer: Sujit Poudel
 * 
 * Program: FileIO module for C++
 * 
 * Objective: This module for C++ makes File IO more
 * easier and accessible. It is supposed to be platform
 * independent as long as you have boost libraries
 * installed.
 * 
 * Last Update: 21st Jan, 2015
 * 
 * License: Free to use and modify.
 * 
 * If you have any comments or concerns, I would be
 * glad to hear them. 
 * Email me at: sujitpoudel123@gmail.com
 * 
 * This is my first program in public repository, and 
 * I would be glad to learn from mistakes I have made
 * here.
 * 
 *********************************************************/
#include "FileIO.h"
void FileIO::CreateDirectory(std::string DirName)
{
	boost::filesystem::path dir(DirName.c_str());
	if(boost::filesystem::create_directories(dir)) 
		std::cout << "Directory Created: " << DirName << "\n";
}


void FileIO::setMode(int FM)
{
	this->FileMode = FM;
}

void FileIO::setFolder(std::string F)
{
	this->Folder = F;
}

void FileIO::setFilename(std::string F)
{
	if (F.length()< 1)	F = "Default.txt";
	this->Filename = F;
}

void FileIO::setFileWithLocation(std::string F)
{
	this->FileWithLocation = F;
}


FileIO::FileIO()
{
	Folder = "Default";
	Filename = "Default.txt";
	FileWithLocation = Folder+"/"+Filename;
	FileMode = Write;
}

FileIO::FileIO(std::string Folder, std::string Filename, int IOMode)
{
	setFolder(Folder);
	if ((!fexists(Folder.c_str())) && (IOMode != Read) && (Folder!= ""))
	{
		CreateDirectory(Folder);
	}
	setFilename(Filename);
	if (getFolder().length() < 1)
		setFileWithLocation(getFilename());
	else
		setFileWithLocation(getFolder()+"/"+getFilename());
	setMode(IOMode);
	
	if (getMode()==Read)
	{
		FileObject.open(getFileWithLocation().c_str(), std::ios::in);
		if (!fexists(getFileWithLocation().c_str()))
			std::cout << "Given file does not exists: " << getFileWithLocation() << std::endl;
	}
	else if (IOMode == Write)
	{
		FileObject.open(getFileWithLocation().c_str(), std::ios::out);
	}
	else if (IOMode == Append)
	{
		FileObject.open(getFileWithLocation().c_str(), std::ios::app);
	}
}

FileIO::~FileIO()
{
	FileObject.close();
}

bool FileIO::fexists(const char* filename)
{
	std::ifstream f(filename);
	if (f.good())
	{
		f.close();
		return true;
	}
	else
	{
		f.close();
		return false;
	}
}

int FileIO::getMode()
{
	return this->FileMode;
}

void FileIO::setFileStatus(int FS)
{
	this->FileReadStatus = FS;
}

int FileIO::getFileStatus()
{
	return this->FileReadStatus;
}

std::string FileIO::getFolder()
{
	return this->Folder;
}

std::string FileIO::getFilename()
{
	return this->Filename;
}

std::string FileIO::getFileWithLocation()
{
	return this->FileWithLocation;
}

std::string FileIO::getLine()
{
	std::string Line = "";
	
	if (this->FileObject.eof())
	{
		setFileStatus(FileIO::FILE_EOF);
		return "EOF";
	}
	if (this->getMode()==Read)
		getline(this->FileObject, Line);
	else
	{
		setFileStatus(FileIO::FILE_NOT_READABLE);
		return "Not readable";
	}
	if (this->FileObject.eof())
	{
		FileReadStatus = FileIO::FILE_EOF;
		return "EOF";
	}
	setFileStatus(FileIO::FILE_READ_INCOMPLETE);
	return Line;
}

std::vector<std::string> FileIO::getAllLines()
{
	FileObject.clear(); // clear bad state after eof
	FileObject.seekg(0); //Reset the position to beginning of file
	setFileStatus(FileIO::FILE_READ_INCOMPLETE);
	if (this->getFileStatus()==FileIO::FILE_EOF) {}
	else if (this->getFileStatus()==FileIO::FILE_READ_INCOMPLETE)
	{
		std::string CurrentLine = this->getLine();
		while (this->getFileStatus()==FileIO::FILE_READ_INCOMPLETE)
		{
			AllLines.push_back(CurrentLine);
			CurrentLine = this->getLine(); 
		}
		FileObject.clear(); // clear bad state after eof
		FileObject.seekg(0); //Reset the position to beginning of file
	}
	return AllLines;
}

std::vector<std::vector<std::string> > FileIO::getAllLinesAsMatrix(char delim)
{
	FileObject.clear(); // clear bad state after eof
	FileObject.seekg(0); //Reset the position to beginning of file
	setFileStatus(FileIO::FILE_READ_INCOMPLETE);
	if (this->getFileStatus()==FileIO::FILE_EOF) {}
	else if (this->getFileStatus()==FileIO::FILE_READ_INCOMPLETE)
	{
		FileObject.clear(); // clear bad state after eof
		FileObject.seekg(0); //Reset the position to beginning of file
		std::string CurrentLine = this->getLine();
		while (this->getFileStatus()==FileIO::FILE_READ_INCOMPLETE)
		{
			AllLinesAsMatrix.push_back(Explode(CurrentLine, delim));
			CurrentLine = this->getLine(); 
		}
		FileObject.clear(); // clear bad state after eof
		FileObject.seekg(0); //Reset the position to beginning of file
	}
	return AllLinesAsMatrix;
}

void FileIO::CloseFile()
{
	this->FileObject.close();
}

void FileIO::ReopenFile(std::string Folder, std::string Filename, int IOMode)
{
	CloseFile();
	setFolder(Folder);
	if ((!fexists(Folder.c_str())) && (IOMode != Read) && (Folder!= ""))
	{
		CreateDirectory(Folder);
	}
	setFilename(Filename);
	if (getFolder().length() < 1)
		setFileWithLocation(getFilename());
	else
		setFileWithLocation(getFolder()+"/"+getFilename());
	setMode(IOMode);
	
	if (getMode()==Read)
	{
		FileObject.open(getFileWithLocation().c_str(), std::ios::in);
	}
	else if (IOMode == Write)
	{
		FileObject.open(getFileWithLocation().c_str(), std::ios::out);
	}
	else if (IOMode == Append)
	{
		FileObject.open(getFileWithLocation().c_str(), std::ios::app);
	}
	
}

void FileIO::SaveData(std::string DataToSave)
{
	if (this->getMode() == Read)
	{
		std::cout << "ReadOnly File: "+getFilename() << std::endl;
	}
	else
	{
		if (this->FileObject.is_open())
		{
			this->FileObject << DataToSave;
		}
		else
		{
			std::cout << "File is not open" << std::endl;
		}
	}
}

std::vector<std::string> FileIO::Explode(std::string const & s, char delim)
{
		std::vector<std::string> result;
		std::istringstream iss(s);
		for (std::string token; std::getline(iss, token, delim); )
		{
			result.push_back(token);
		}
		return result;
}
