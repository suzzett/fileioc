# The variable CC will be
# the compiler to use.
CC=g++
# We need boost system and filesystem library for this to compile
CFLAGS=-Wall -lboost_system -lboost_filesystem -lboost_iostreams
CFLAGS_I=-c -Wall -lboost_system -lboost_filesystem

all:	Test

Lib:	FileIO.o

Test:	Test.o	FileIO.o
	$(CC)	$(CFLAGS)	FileIO.o	Test.o	-o	Test

Test.o:	FileIO.h	Test.cc
	$(CC)	$(CFLAGS_I)	Test.cc

FileIO.o:	FileIO.h	FileIO.cc
	$(CC)	$(CFLAGS_I)	FileIO.cc

clean:	
	rm *.o Test