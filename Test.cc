#include "FileIO.h"
#include<sstream>
#define FOLDER 		"SampleFolder/Test"
#define FILENAME 	"Input.txt"

int main()
{
	FileIO OutputFile(FOLDER, FILENAME, FileIO::Write);
	std::cout << "Writing to file." << std::endl;
	for (unsigned int i = 1; i <= 10; i++)
	{
		std::ostringstream ss1;
		ss1 << std::fixed << i;
		std::ostringstream ss2;
		ss2 << std::fixed << (i*123.12);
		std::ostringstream ss3;
		ss3 << std::fixed << 123.12;
		std::string LineValue = ss3.str() +" X " + ss1.str()+ " = " + ss2.str();
		OutputFile.SaveData(LineValue+"\n");
		std::cout << "Saved: " << LineValue << std::endl;
	}
	OutputFile.CloseFile();
	FileIO InputFile(FOLDER, FILENAME, FileIO::Read);
	std::string Line = InputFile.getLine();
	std::cout << "Reading from file." << std::endl;
	while (InputFile.getFileStatus() == FileIO::FILE_READ_INCOMPLETE)
	{
		std::cout << Line << std::endl;
		Line = InputFile.getLine();
	}
	std::cout << "===================================================" << std::endl;
	std::vector<std::string> AllLines = InputFile.getAllLines();
	for (unsigned int i = 0; i < AllLines.size(); i++)
	{
		std::cout << "Line " << (i+1) << ": " << AllLines[i] << std::endl;
	}
	std::cout << "===================================================" << std::endl;

	std::vector<std::vector<std::string> > AllLinesAsMatrix = InputFile.getAllLinesAsMatrix('.');
	for (unsigned int i = 0; i < AllLinesAsMatrix.size(); i++)
	{
		std::cout << "Line " << (i+1) << ": ";
		for (unsigned int j = 0; j < AllLinesAsMatrix[i].size(); j++)
		{
			std::cout << (((j==0)?"":",")+AllLinesAsMatrix[i][j]);
		}
		std::cout << std::endl;
	}
	InputFile.CloseFile();
	return 0;
}