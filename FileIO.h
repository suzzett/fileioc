#ifndef FILEIO_H
#define FILEIO_H

#include<fstream>
#include<sstream>
#include<iostream>
//Requires to link -lboost_system and -lboost_filesystem for compiler
#include <boost/filesystem.hpp>

class FileIO
{
	private:
		std::string Folder;
		std::string Filename;
		std::string FileWithLocation;
		int FileMode;
		std::fstream FileObject;
		void setMode(int FM);
		void setFileStatus(int FS);
		void setFolder(std::string F);
		void setFilename(std::string F);
		void setFileWithLocation(std::string F);
		std::vector<std::string> AllLines;
		std::vector<std::vector<std::string> > AllLinesAsMatrix;
		int FileReadStatus;

	public:
		enum FILE_READ_STATUS {FILE_EOF, FILE_READ_INCOMPLETE, FILE_NOT_READABLE};
		enum FileIOMode{Read, Write, Append};
		FileIO();
		FileIO(std::string Folder, std::string Filename, int IOMode);
		~FileIO();
		bool fexists(const char* filename);
		int getMode();
		std::string getFolder();

		int getFileStatus();
		void CreateDirectory(std::string DirName);
		std::string getFilename();
		std::string getFileWithLocation();
		std::string getLine();
		std::vector<std::string> getAllLines();
		std::vector<std::vector<std::string> > getAllLinesAsMatrix(char delim);
		void CloseFile();
		void ReopenFile(std::string Folder, std::string Filename, int IOMode);
		void SaveData(std::string DataToSave);
		std::vector<std::string> Explode(std::string const & s, char delim);
};

#endif