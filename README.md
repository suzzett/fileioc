# README #

### What is this repository for? ###

* This is simple C++ object to make File IO easier using boost library
* This is my first code in public repository.
* Free to use and modify
* Version 0.1 beta

### Requirements:###
* Boost Library needs to be installed in the system
* Tested on Linux only (Fedora system)

###How to run tests###
* Use makefile provided

### Who do I talk to? ###

* Repo owner: Sujit Poudel
* Any comments or suggestions contact me at: sujitpoudel123@gmail.com